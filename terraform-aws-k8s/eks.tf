resource "aws_iam_role" "eks_cluster" {
  # The name of the role
  name = "eks-cluster"

  # The policy that grants an entity permission to assume the role.
  # Used to access AWS resources that you might not normally have access to.
  # The role that Amazon EKS will use to create AWS resources for Kubernetes clusters
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }

  ]
}
POLICY
}


resource "aws_iam_role_policy_attachment" "amazon_eks_cluster_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  # The role the policy should be applied to
  role = aws_iam_role.eks_cluster.name
}
resource "aws_iam_role_policy_attachment" "amazon_elb_policy_general" {
  # The ARN of the policy you want to apply.
  policy_arn = "arn:aws:iam::aws:policy/ElasticLoadBalancingFullAccess"
  # The role the policy should be applied to
  role = aws_iam_role.eks_cluster.name
}

resource "aws_iam_role_policy_attachment" "amazon_ec2_policy_general" {
  # The ARN of the policy you want to apply.
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"

  role = aws_iam_role.eks_cluster.name
}

resource "aws_eks_cluster" "eks" {

  name = "eks-test"


  role_arn = aws_iam_role.eks_cluster.arn


  version = "1.18"

  vpc_config {

    endpoint_private_access = false

    endpoint_public_access = true

    subnet_ids = [
      aws_subnet.public-01.id,
      aws_subnet.private-01.id,
      aws_subnet.public-02.id,
      aws_subnet.private-02.id
    ]
  }

  depends_on = [
    aws_iam_role_policy_attachment.amazon_eks_cluster_policy
  ]
}
