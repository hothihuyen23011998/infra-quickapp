
resource "aws_subnet" "public-01" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "172.16.0.0/18"

  availability_zone = "us-east-2a"
  tags = {
    Name = "public-us-east-2a"
    "kubernetes.io/cluster/eks-test" = "shared"
    "kubernetes.io/role/elb"         = 1
  }
}

resource "aws_subnet" "public-02" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "172.16.64.0/18"
  
  availability_zone = "us-east-2b"
  tags = {
    Name = "public-us-east-2b"
    "kubernetes.io/cluster/eks-test" = "shared"
    "kubernetes.io/role/elb"         = 1
  }
}

resource "aws_subnet" "private-01" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "172.16.128.0/18"

  availability_zone = "us-east-2a"
  tags = {
    Name = "private-us-east-2a"
    "kubernetes.io/cluster/eks-test" = "shared"
    "kubernetes.io/role/elb"         = 1
  }
}

resource "aws_subnet" "private-02" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "172.16.192.0/18"

  availability_zone = "us-east-2a"
  tags = {
    Name = "private-us-east-2b"
    "kubernetes.io/cluster/eks-test" = "shared"
    "kubernetes.io/role/elb"         = 1
  }
}


