#!/bin/bash
export ENVIRONMENT="Production"
echo "ENVIRONMENT"

sudo apt-get install ec2-instance-connect -y

echo "Install java"
sudo apt install default-jre -y

echo "Install unzip"
sudo apt install  unzip -y

echo "Install dotnet core"
wget https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb

sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y dotnet-sdk-5.0

sudo apt-get update; \
  sudo apt-get install -y aspnetcore-runtime-5.0

sudo apt-get install -y dotnet-runtime-5.0

sudo usermod -aG sudo ubuntu
sudo usermod -aG root ubuntu
sudo usermod -aG adm ubuntu

touch QuickApp.service
echo "Environment = $ENVIRONMENT"
echo "Generate QuickApp service file"
cat >> QuickApp.service << EOF
[Unit]
Description=QuickApp .NET Web API App running on Ubuntu
    
[Service]
WorkingDirectory=/home/ubuntu/deploy-be
ExecStartPre=/usr/bin/sudo /usr/bin/dotnet dev-certs https --trust
ExecStart=/usr/bin/sudo /usr/bin/dotnet /home/ubuntu/deploy-be/QuickApp.dll
Restart=always
# Restart service after 10 seconds if the dotnet service crashes:
RestartSec=10
User=root
Environment=ASPNETCORE_ENVIRONMENT=$ENVIRONMENT

[Install]
WantedBy=multi-user.target
EOF

sudo touch /etc/systemd/system/QuickApp.service
sudo chown -R ubuntu:ubuntu /etc/systemd/system/QuickApp.service
sudo cp QuickApp.service /etc/systemd/system/QuickApp.service
sudo rm QuickApp.service
sudo systemctl daemon-reload
sudo systemctl enable QuickApp
#sudo systemctl start QuickApp
