#!/bin/bash

echo "install java and ec2-instance-connect"
sudo apt update
sudo apt install nginx unzip -y
sudo systemctl enable nginx 
sudo systemctl start nginx
sudo apt-get install ec2-instance-connect -y
sudo apt install default-jre -y

sudo apt install certbot python3-certbot-nginx -y

sudo rm /var/www/html/*
sudo rm /etc/nginx/site-enabled/*
# sudo chown -R ubuntu:ubuntu /etc/nginx/site-enabled
# sudo touch /etc/nginx/site-enabled/quickapp-be

# sudo cat >> /etc/nginx/site-enabled/quickapp-be << EOF
# server {
#     server_name huyen-productionbe.byhanh.com;
#     location / {
#     proxy_pass https://localhost:8002;
#     proxy_http_version 1.1;
#     proxy_set_header Upgrade $http_upgrade;
#     proxy_set_header Connection 'upgrade';
#     proxy_set_header Host $host;
#     proxy_cache_bypass $http_upgrade;
# }

# EOF

# # sudo certbot run -n --nginx --agree-tos -d huyen-be.byhanh.com -m hothihuyen23011998@gmail.com --redirect
# sudo touch /etc/nginx/site-enabled/quickapp-fe


# sudo cat >> /etc/nginx/site-enabled/quickapp-fe << EOF
# server {
# 	root /var/www/html;

# 	# Add index.php to the list if you are using PHP
# 	index index.html index.htm index.nginx-debian.html;
#     server_name huyen-productionfe.byhanh.com; 
# }


# EOF

# certbot run -n --nginx --agree-tos -d huyen-productionbe.byhanh.com -m hothihuyen23011998@gmail.com --redirect
# certbot run -n --nginx --agree-tos -d huyen-productionfe.byhanh.com -m hothihuyen23011998@gmail.com --redirect

sudo nginx -s reload
