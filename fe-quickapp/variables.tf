variable "instance_name" {
    description = "Name of instance"
}

variable "ami_id" {
    description = "Name of ami"
}

variable "key_name" {
    description = "Name of key"
}

#ami-0b9064170e32bde34
variable "cloudflare_email" {
  description = "The email associated with the account. This can also be specified with the CLOUDFLARE_EMAIL shell environment variable."
  default     = "default"
}

variable "api_token" {
  description = "The Cloudflare API token. This can also be specified with the CLOUDFLARE_TOKEN shell environment variable."
  default     = "default"
}

variable "record_name" {
  description = "The name of record"
  default     = "default"
}

variable "zone_id" {
  description = "Zone ID"
  default     = "default"
}
