terraform {
    required_version = ">= 0.14.0"

    required_providers {
        cloudflare = {
            source = "registry.terraform.io/cloudflare/cloudflare"
            version = "~> 2"
        }

        aws = {
            source = "hashicorp/aws"
            version = "~> 3"
        }
    }
}

provider "aws" {
   region = "us-east-2"
}

resource "aws_instance" "web" {
  ami           = "${var.ami_id}"
  instance_type = "t2.micro"
  security_groups = ["Allow web traffic"]
  key_name = "${var.key_name}"
  #user_data = "${file("server-strip.sh")}"

  tags = {
    Name = "${var.instance_name}"
  }
}

resource "aws_eip" "web_ip" {
  instance = aws_instance.web.id
}


#resource "aws_key_pair" "my_key" {
# key_name = "${var.key_name}"
# public_key = "${file(".ssh/id_rsa.pub")}"
#}
#"${aws_key_pair.my_key.key_name}"


output "BackendPublicIP" {
  value = aws_eip.web_ip.public_ip
  
}

provider "cloudflare" {
  email = "hothihuyen23011998@gmail.com"
  api_token = "${var.api_token}"
}

resource "cloudflare_record" "terraform" {
 zone_id = "${var.zone_id}"
 name = "${var.record_name}"
 value = "${aws_eip.web_ip.public_ip}"
 type = "A"
 ttl = 1
}
