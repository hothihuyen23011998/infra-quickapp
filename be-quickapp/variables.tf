variable "instance_name" {
    description = "Name of instance"
}

variable "ami_id" {
    description = "Name of ami"
}
variable "key_name" {
    description = "Name of key"
}


variable "cloudflare_email" {
  description = "The email associated with the account. This can also be specified with the CLOUDFLARE_EMAIL shell environment variable."
  default     = "default"
}

variable "api_token" {
  description = "The Cloudflare API token. This can also be specified with the CLOUDFLARE_TOKEN shell environment variable."
  default     = "default"
}

#variable "site_domain" {
#  description = "The DNS zone to add the record to."
#  default     = "default"
#}

variable "record_name" {
  description = "The DNS zone to add the record to."
  default     = "default"
}

variable "zone_id" {
  description = "Zone ID"
  default     = "default"
}