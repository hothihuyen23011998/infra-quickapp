terraform {
    required_version = ">= 0.14.0"

    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 3"
        }
    }
}

provider "aws" {
   region = "us-east-2"
   profile = "hohuyen"
}

resource "aws_ecr_repository" "fe" {
  name = "my-quickapp-fe"
}

resource "aws_ecr_repository" "be" {
  name = "my-quickapp-be"
}

output "quickapp-frontend-repositor-URL" {
  value = "${aws_ecr_repository.fe.repository_url}"
}

output "quickapp-backend-repositor-URL" {
  value = "${aws_ecr_repository.be.repository_url}"
}
