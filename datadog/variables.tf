variable "datadog_api_key" {
  description = "The datadog api key"
  default     = "default"
}

variable "datadog_app_key" {
  description = "The datadog app key"
  default     = "default"
}

variable "environment" {
  description = "Name of environment"
  default     = "default"
}


variable "service" {
  description = "Name of service"
  default     = "default"
}


variable "host" {
  description = "Host IP"
  default     = "default"
}