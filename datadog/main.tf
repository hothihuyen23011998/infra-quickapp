terraform {
  required_providers {
    datadog = {
      source = "DataDog/datadog"
    }
  }
}

provider "datadog" {
  api_key = "${var.datadog_api_key}"
  app_key = "${var.datadog_app_key}"
}

resource "datadog_monitor" "process_alert_example" {
  name    = "Process Alert Monitor"
  type    = "process alert"
  message = "Multiple Java processes running on example-tag. Notify: @slack-Devops-quickapp"
  query   = "processes('java').over('example-tag').rollup('count').last('10m') > 1"
  monitor_thresholds {
    critical          = 1.0
    critical_recovery = 0.0
  }

  notify_no_data    = false
  renotify_interval = 60
}

resource "datadog_monitor" "ubuntu-02" {
  name               = "Monitor ubuntu 02"
  type               = "metric alert"
  message            = "Monitor triggered. Notify: @slack-Devops-quickap"

  query = "avg(last_5m):avg:system.disk.used{*} > 21474836480"

  monitor_thresholds {
    warning           = 16106127360
    warning_recovery  = 13958643712
    critical          = 21474836480
    critical_recovery = 20401094656
  }

  notify_no_data    = false
  renotify_interval = 60

  notify_audit = false
  timeout_h    = 60
  include_tags = true

  # ignore any changes in silenced value; using silenced is deprecated in favor of downtimes
  lifecycle {
    ignore_changes = [silenced]
  }

}


