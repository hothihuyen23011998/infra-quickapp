terraform {
    required_version = ">= 0.14.0"

    required_providers {
        newrelic = {
      source  = "newrelic/newrelic"
      version = "~> 2.21.0"
    }
    }
}

provider "newrelic" {}

resource "newrelic_alert_policy" "my-quickapp-test-policy" {
  name = "My quickapp Policy"
}

data "newrelic_entity" "app" {
  name = "my-quickapp"
  # domain = "INFRA"
  # type = "MONITOR"
  
}


resource "newrelic_infra_alert_condition" "high_cpu" {
  policy_id   = newrelic_alert_policy.my-quickapp-test-policy.id
  name        = "High CPU usage"
  type        = "infra_metric"
  event       = "SystemSample"
  select      = "cpuPercent"
  comparison  = "above"
  where       = "(`Entity guid` = '${data.newrelic_entity.app.id}')"

  # Define a critical alert threshold that will trigger after 5 minutes above 90%
  critical {
    duration      = 5
    value         = 90
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "high_disk_usage" {
  policy_id = newrelic_alert_policy.my-quickapp-test-policy.id

  name        = "High disk usage"
  description = "Warning if disk usage goes above 80% and critical alert if goes above 90%"
  type        = "infra_metric"
  event       = "StorageSample"
  select      = "diskUsedPercent"
  comparison  = "above"
  where       = "(`Entity guid` = '${data.newrelic_entity.app.id}')"

  critical {
    duration      = 5
    # 25 vs 90
    value         = 10
    time_function = "all"
  }

  warning {
    duration      = 2
    #10 vs 80
    value         = 8
    time_function = "all"
  }
}

resource "newrelic_infra_alert_condition" "high_db_conn_count" {
  policy_id = newrelic_alert_policy.my-quickapp-test-policy.id

  name        = "High database connection count"
  description = "Critical alert when the number of database connections goes above 90"
  type        = "infra_metric"
  event       = "DatastoreSample"
  select      = "provider.databaseConnections.Average"
  comparison  = "above"
  where       = "(`Entity guid` = '${data.newrelic_entity.app.id}')"
  integration_provider = "RdsDbInstance"

  critical {
    duration      = 25
    value         = 90
    time_function = "all"
  }
}


# Slack notification channel
resource "newrelic_alert_channel" "slack_notification" {
  name = "slack-quickapp"
  type = "slack"

  config {
    url     = "https://hooks.slack.com/services/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"
    channel = "quickapp-relic"
  }
}

resource "newrelic_alert_policy_channel" "slack_alert_policy" {
  policy_id = newrelic_alert_policy.my-quickapp-test-policy.id
  channel_ids = [
    newrelic_alert_channel.slack_notification.id,
  ]
  
}
