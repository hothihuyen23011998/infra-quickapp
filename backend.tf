terraform {
    required_version = ">= 0.14.0"

    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 3"
        }
    }
}

provider "aws" {
   region = "us-east-2"
}

resource "aws_s3_bucket" "my_static_site" {
   bucket = "tfstate-huyenht-docker-ecr-ecs-123123123"
   acl    = "private"
}

resource "aws_s3_bucket" "my_static_site-2" {
   bucket = "tfstate-huyenht-123123123"
   acl    = "private"
}

output "TFSTATE_BUCKET_NAME" {
  value = aws_s3_bucket.my_static_site.bucket
}

output "TFSTATE_BUCKET_NAME-2" {
  value = aws_s3_bucket.my_static_site-2.bucket
}