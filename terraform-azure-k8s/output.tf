
output "client_certificate" {
    value = azurerm_kubernetes_cluster.k8s.kube_config.0.client_certificate
}

output "kube_config" {
    value = azurerm_kubernetes_cluster.k8s.kube_config_raw
    sensitive = true
}

output "static_ip" {
    value = azurerm_public_ip.k8s.ip_address
}

output "full_dns" {
  value = azurerm_public_ip.k8s.name
}

output "resource_name" {
  value = azurerm_resource_group.my-k8s.name
}

output "cluster_name" {
  value = azurerm_kubernetes_cluster.k8s.name
}

output "dns_label" {
    value = azurerm_public_ip.k8s.domain_name_label
}
