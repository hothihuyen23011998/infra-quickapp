provider "azurerm" {
    version = "~>2.0"
    features {}
}

resource "azurerm_resource_group" "my-k8s" {
    name     = "${var.resource_group_name}"
    location = "${var.location}"
}

resource "azurerm_kubernetes_cluster" "k8s" {
    name                = "${var.cluster_name}"
    location            = azurerm_resource_group.my-k8s.location
    resource_group_name = azurerm_resource_group.my-k8s.name
    node_resource_group = "my-quickapp-node-resource"
    dns_prefix          = "${var.dns_prefix}"
    identity {
    type = "SystemAssigned"
  }
    linux_profile {
        admin_username = "ubuntu"

        ssh_key {
            key_data = "${file(".ssh/id_rsa.pub")}"
        }
    }

    default_node_pool {
        name            = "agentpool"
        node_count      = 1
        vm_size         = "Standard_B2s" 
    }

    tags = {
        Environment = "Staging"
    }
}

resource "azurerm_public_ip" "k8s" {
  name                = "quickapp-test"
  resource_group_name = azurerm_kubernetes_cluster.k8s.node_resource_group
  location            = azurerm_resource_group.my-k8s.location
  allocation_method   = "Static"
  idle_timeout_in_minutes = 30
  ip_version = "IPv4"
  domain_name_label = "my-quickapp-test-k8s"
  sku = "Standard"
  tags = {
    environment = "Staging"
  }
}