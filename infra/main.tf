terraform {
    required_version = ">= 0.14.0"

    required_providers {
        cloudflare = {
            source = "registry.terraform.io/cloudflare/cloudflare"
            version = "~> 2"
        }

        aws = {
            source = "hashicorp/aws"
            version = "~> 3"
        }
    }
}

provider "aws" {
   region = "us-east-2"
}

resource "aws_instance" "web" {
  ami           = "ami-0629615e9faee380c"
  instance_type = "t2.micro"
  security_groups = ["Allow web traffic"]
  key_name = "${aws_key_pair.my_key.key_name}"

  tags = {
    Name = "${var.instance_name}"
  }
}

resource "aws_key_pair" "my_key" {
 key_name = "my quickappp key"
 public_key = "${file(".ssh/id_rsa.pub")}"
}
#


resource "aws_eip" "web_ip" {
  instance = aws_instance.web.id
}

output "PublicIP" {
  value = aws_eip.web_ip.public_ip
  
}
